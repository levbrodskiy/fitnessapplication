package ru.iei.fitnessapp.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iei.fitnessapp.db.entities.FitnessStage;

@Repository
public interface FitnessStageRepository extends JpaRepository<FitnessStage, Long> {
}
