package ru.iei.fitnessapp.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iei.fitnessapp.db.entities.UserFitnessStage;

import java.util.List;

@Repository
public interface UserFitnessStageRepository extends JpaRepository<UserFitnessStage, Long> {
    List<UserFitnessStage> findAllByUserId(@Param("userID") long userID);
}
