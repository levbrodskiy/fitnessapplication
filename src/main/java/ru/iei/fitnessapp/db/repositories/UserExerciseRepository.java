package ru.iei.fitnessapp.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iei.fitnessapp.db.entities.UserExercise;

@Repository
public interface UserExerciseRepository extends JpaRepository<UserExercise, Long> {
}
