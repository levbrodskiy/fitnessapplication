package ru.iei.fitnessapp.db.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "users_fitness_stages")
public class UserFitnessStage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "number")
    private Integer number;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private FitnessStageStatus status;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "userFitnessStage", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<UserExercise> userExercises;

    public UserExercise getFirstUserExercise() {
        if (userExercises.size() < 1) {
            return null;
        }

        return userExercises.get(0);
    }

    public UserExercise getUserExerciseByNumber(int number) {
        for (UserExercise exercise : userExercises) {
            if (exercise.getNumber().equals(number)) {
                return exercise;
            }
        }

        return null;
    }
}
