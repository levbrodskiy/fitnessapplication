package ru.iei.fitnessapp.db.entities;

public enum  ComplexityLevel {
    EASY, MIDDLE, HARD
}
