package ru.iei.fitnessapp.db.entities;

public enum FitnessStageStatus {
    COMPLETED, CLOSE, IN_PROCESS
}
