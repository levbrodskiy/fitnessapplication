package ru.iei.fitnessapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.iei.fitnessapp.db.entities.User;
import ru.iei.fitnessapp.services.UserService;

@Controller
@RequestMapping("/signin")
public class SingInController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String getSignInPage() {
        return "signin";
    }

    @PostMapping
    public String singIn(@RequestParam(name = "email") String email,
                        @RequestParam(name = "password") String password) {

        User user = null;
        try {
            user = userService.authorize(email, password);
            System.out.println("Good@@@@@@@@@@@@@@@@@@@@@@@@@");
        } catch (Exception e) {
            System.out.println("NOOOOOOOOOOOSSODIFIFJPOKDFPOJDOJIDOH");
            return "error";
        }
        return "redirect:/user" + user.getId() + "/stages";
    }
}
