package ru.iei.fitnessapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.iei.fitnessapp.controllers.responses.StageFinishResponse;
import ru.iei.fitnessapp.controllers.responses.UserResponse;
import ru.iei.fitnessapp.services.UserFitnessStageService;
import ru.iei.fitnessapp.services.UserService;

import java.util.List;


@Controller
public class HomeController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserFitnessStageService userFitnessStageService;

    @GetMapping("/user{user_id}/home")
    public String getHomePage(@PathVariable(name = "user_id") Long userID,
                              Model model) {
        UserResponse response = userService.getUserResponse(userID);
        model.addAttribute("user", response);
        return "home";
    }

    @GetMapping("/user{user_id}/course_results")
    public String getCourseResultPage(@PathVariable(name = "user_id") Long userID,
                                      Model model) {

        List<StageFinishResponse> responses = userFitnessStageService.getStageFinishResponses(userID);
        model.addAttribute("courseResults", responses);

        return "course_results";
    }
}
