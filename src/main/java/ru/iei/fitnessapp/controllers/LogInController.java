package ru.iei.fitnessapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.iei.fitnessapp.db.entities.User;
import ru.iei.fitnessapp.services.UserService;
import ru.iei.fitnessapp.services.exceptions.RegistrationException;

@Controller
@RequestMapping("/login")
public class LogInController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String getLogInPage() {
        return "login";
    }

    @PostMapping
    public String logIn(@RequestParam(name = "email") String name,
                        @RequestParam(name = "password") String password,
                        @RequestParam(name = "level") String level,
                        Model model) {

        User user = null;
        try {
            user = userService.register(name, password, level);
        } catch (Exception e) {
            return "error";
        }

        return "redirect:/user" + user.getId() + "/home";
    }
}
