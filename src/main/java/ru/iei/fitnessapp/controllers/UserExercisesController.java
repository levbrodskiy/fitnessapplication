package ru.iei.fitnessapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.iei.fitnessapp.controllers.responses.StageFinishResponse;
import ru.iei.fitnessapp.controllers.responses.UserExerciseResponse;
import ru.iei.fitnessapp.controllers.responses.UserFitnessStageResponse;
import ru.iei.fitnessapp.db.entities.UserExercise;
import ru.iei.fitnessapp.db.entities.UserFitnessStage;
import ru.iei.fitnessapp.exceptions.UserFitnessStageNotFoundException;
import ru.iei.fitnessapp.services.UserExercisesService;
import ru.iei.fitnessapp.services.UserFitnessStageService;
import ru.iei.fitnessapp.services.exceptions.UserExerciseNotFoundException;

@Controller
public class UserExercisesController {

    @Autowired
    private UserFitnessStageService userFitnessStageService;
    @Autowired
    private UserExercisesService userExercisesService;

    @GetMapping("/user{user_id}/stages/stage{stage_id}/start")
    public String getStartPage(@PathVariable(name = "user_id") Long userID,
                               @PathVariable(name = "stage_id") Long stageID,
                               Model model) {
        try {
            UserExerciseResponse response = userFitnessStageService.getFirstExerciseResponse(stageID);
            model.addAttribute("exercise", response);
            return "exercise";
        } catch (UserFitnessStageNotFoundException e) {
            return "error";
        }
    }

    //http://localhost:8080/user8/stages/stage19/next-exercise1?miss=false
    //http://localhost:8080/user8/stages/stage19/next-exercise1?miss=false
    @GetMapping("/user{user_id}/stages/stage{stage_id}/next-exercise{exercise_number}")
    public String getNextExercisePage(@PathVariable(name = "user_id") Long userID,
                                      @PathVariable(name = "stage_id") Long stageID,
                                      @PathVariable(name = "exercise_number") Integer exerciseNumber,
                                      @RequestParam(name = "miss", defaultValue = "null") Boolean miss,
                                      Model model) throws UserExerciseNotFoundException {
        try {
            UserExerciseResponse response = userFitnessStageService.getNextUserExerciseResponseByNumber(stageID, exerciseNumber);
            UserExercise exercise = userFitnessStageService.getExerciseByStageIDAndNumber(stageID, exerciseNumber);

            if (miss != null) {
                userExercisesService.updateMissing(miss,exercise.getId());
            }

            if (response == null) {
                StageFinishResponse responseF = userFitnessStageService.getStageFinishResponse(stageID);
                System.out.println("response == null");
                model.addAttribute("stage", responseF);
                return "exerciseResult";
            }

            model.addAttribute("exercise", response);
            return "exercise";


        } catch (UserFitnessStageNotFoundException e) {
            return "error";
        }

    }
}
