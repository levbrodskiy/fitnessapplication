package ru.iei.fitnessapp.controllers.responses;

import lombok.Data;

@Data
public class StageFinishResponse {
    private String name;
    private Long stageID;
    private Long userID;
    private Integer missingExerciseCount;
    private Double assessment;
    private Integer stageNumber;
}
