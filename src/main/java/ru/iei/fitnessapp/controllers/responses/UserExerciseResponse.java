package ru.iei.fitnessapp.controllers.responses;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserExerciseResponse {
    private Long id;
    private Long userID;
    private Long stageID;
    private String name;
    private String description;
    private Boolean missing;
    private Integer number;
}
