package ru.iei.fitnessapp.controllers.responses;

import lombok.Data;

@Data
public class UserResponse {
    private Long userID;
    private String email;
}
