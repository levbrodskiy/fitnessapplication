package ru.iei.fitnessapp.controllers.responses;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserFitnessStageResponse {
    private Long stageID;
    private String name;
    private Integer number;
    private String description;
    private Long userID;
}
