package ru.iei.fitnessapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.iei.fitnessapp.services.UserFitnessStageService;

@Controller
public class UsersFitnessStagesController {
    @Autowired
    private UserFitnessStageService userFitnessStageService;

    @GetMapping("/user{user_id}/stages")
    public String getUserFitnessStages(@PathVariable(name = "user_id") Long userID,
                                       Model model) {

        model.addAttribute("stages",
                userFitnessStageService.getAllUserStageResponse(userID));

        return "allSteps";
    }

    @GetMapping("/user{user_id}/stages/stage{stage_id}")
    public String getStage(@PathVariable(name = "user_id") Long userID,
                           @PathVariable(name = "stage_id") Long stageID,
                           Model model) {

        model.addAttribute("stage",
                userFitnessStageService.getStageResponse(stageID));
        return "stage";
    }
}
