package ru.iei.fitnessapp.utils;

import ru.iei.fitnessapp.db.entities.ComplexityLevel;
import ru.iei.fitnessapp.utils.exceptions.BadArgumentException;

public class ComplexityLevelUtils {
    public static ComplexityLevel getComplexityLevel(String complexityLevel) throws BadArgumentException {

        for (ComplexityLevel level : ComplexityLevel.values()) {

            if (level.toString().equals(complexityLevel)) {

                return level;

            }

        }

        throw new BadArgumentException();
    }
}
