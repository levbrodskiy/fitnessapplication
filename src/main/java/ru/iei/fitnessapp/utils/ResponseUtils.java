package ru.iei.fitnessapp.utils;

import ru.iei.fitnessapp.controllers.responses.UserExerciseResponse;
import ru.iei.fitnessapp.controllers.responses.UserFitnessStageResponse;
import ru.iei.fitnessapp.controllers.responses.UserResponse;
import ru.iei.fitnessapp.db.entities.User;
import ru.iei.fitnessapp.db.entities.UserExercise;
import ru.iei.fitnessapp.db.entities.UserFitnessStage;

public class ResponseUtils {
    public static UserFitnessStageResponse getStageResponse(UserFitnessStage stage) {
        return UserFitnessStageResponse.
                builder().
                stageID(stage.getId()).
                name(stage.getName()).
                description(stage.getDescription()).
                userID(stage.getUser().getId()).
                number(stage.getNumber()).
                build();
    }


    public static UserExerciseResponse getUserExerciseResponse(UserExercise exercise, long userID) {
        return UserExerciseResponse.builder().
                id(exercise.getId()).
                name(exercise.getName()).
                description(exercise.getDescription()).
                number(exercise.getNumber()).
                stageID(exercise.getUserFitnessStage().getId()).
                missing(exercise.getMissing()).
                userID(userID).
                build();
    }

    public static UserResponse getUserResponse(User user) {
        UserResponse userResponse = new UserResponse();
        userResponse.setEmail(user.getUsername());
        userResponse.setUserID(user.getId());
        return userResponse;
    }
}
