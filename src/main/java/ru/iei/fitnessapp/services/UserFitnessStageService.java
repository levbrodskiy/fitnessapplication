package ru.iei.fitnessapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iei.fitnessapp.controllers.responses.StageFinishResponse;
import ru.iei.fitnessapp.controllers.responses.UserExerciseResponse;
import ru.iei.fitnessapp.controllers.responses.UserFitnessStageResponse;
import ru.iei.fitnessapp.db.entities.FitnessStageStatus;
import ru.iei.fitnessapp.db.entities.User;
import ru.iei.fitnessapp.db.entities.UserExercise;
import ru.iei.fitnessapp.db.entities.UserFitnessStage;
import ru.iei.fitnessapp.db.repositories.UserFitnessStageRepository;
import ru.iei.fitnessapp.db.repositories.UserRepository;
import ru.iei.fitnessapp.exceptions.UserFitnessStageNotFoundException;
import ru.iei.fitnessapp.services.exceptions.UserExerciseNotFoundException;
import ru.iei.fitnessapp.utils.ResponseUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserFitnessStageService {
    @Autowired
    private UserFitnessStageRepository userFitnessStageRepository;
    @Autowired
    private UserRepository userRepository;

    public UserFitnessStageResponse getStageResponse(long stageID) {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);

        return ResponseUtils.getStageResponse(stage);
    }

    public List<UserFitnessStageResponse> getAllUserStageResponse(long userID) {
        List <UserFitnessStageResponse> result = new ArrayList<>();

        for (UserFitnessStage stage : userFitnessStageRepository.findAllByUserId(userID)) {
            result.add(ResponseUtils.getStageResponse(stage));
        }

        return result;
    }

    public List<UserExercise> getAllUserExercisesByUserStageID(long id) {
        return null;
    }

    public void updateStatus(long userFitnessStageID, FitnessStageStatus status) throws UserFitnessStageNotFoundException {
        UserFitnessStage stage = userFitnessStageRepository.getOne(userFitnessStageID);

        if (stage == null) {
            throw new UserFitnessStageNotFoundException();
        }

        stage.setStatus(status);

        userFitnessStageRepository.save(stage);
    }

    public UserExerciseResponse getFirstExerciseResponse(long stageID) throws UserFitnessStageNotFoundException {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);
        if (stage == null) {
            throw new UserFitnessStageNotFoundException();
        }
        UserExercise exercise = stage.getFirstUserExercise();
        return ResponseUtils.getUserExerciseResponse(exercise, stage.getUser().getId());
    }

    public UserExerciseResponse getNextUserExerciseResponseByNumber(long stageID, int exerciseNumber) throws UserFitnessStageNotFoundException, UserExerciseNotFoundException {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);
        if (stage == null) {
            throw new UserFitnessStageNotFoundException();
        }

        exerciseNumber += 1;
        UserExercise exercise = stage.getUserExerciseByNumber(exerciseNumber);


        if (exercise == null) {
            return null;
        }

        return ResponseUtils.getUserExerciseResponse(exercise, stage.getUser().getId());
    }

    public UserExercise getExerciseByStageIDAndNumber(long stageID, int number) {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);
        return stage.getUserExerciseByNumber(number);
    }

    public StageFinishResponse getStageFinishResponse(long stageID) {
        UserFitnessStage stage = userFitnessStageRepository.getOne(stageID);

        StageFinishResponse response = new StageFinishResponse();
        response.setStageID(stageID);
        response.setName(stage.getName());
        response.setStageNumber(stage.getNumber());

        int missCount = 0;
        int allCount = stage.getUserExercises().size();

        for (UserExercise exercise : stage.getUserExercises()) {
            if (exercise.getMissing() != null && exercise.getMissing()) {
                missCount++;
            }
        }
        double elem = 100 / allCount;
        double missElements = elem * missCount;
        double assessment = 100 - missElements;

        response.setMissingExerciseCount(missCount);
        response.setAssessment(assessment);
        response.setUserID(stage.getUser().getId());
        return response;
    }

    public List<StageFinishResponse> getStageFinishResponses(long userID) {
        List<StageFinishResponse> responses = new ArrayList<>();

        User user = userRepository.getOne(userID);

        if (user == null) {
            return null;
        }

        for (UserFitnessStage stage : user.getUserFitnessStages()) {
            responses.add(getStageFinishResponse(stage.getId()));
        }

        return responses;
    }
}
