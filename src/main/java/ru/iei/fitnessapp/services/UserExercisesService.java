package ru.iei.fitnessapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iei.fitnessapp.db.entities.UserExercise;
import ru.iei.fitnessapp.db.repositories.UserExerciseRepository;

@Service
public class UserExercisesService {
    @Autowired
    private UserExerciseRepository userExerciseRepository;

    public UserExercise getByID(long userExerciseID) {
    return userExerciseRepository.getOne(userExerciseID);
    }

    public void updateMissing(Boolean miss, long userExerciseID) {
        UserExercise exercise = userExerciseRepository.getOne(userExerciseID);
        exercise.setMissing(miss);
        userExerciseRepository.saveAndFlush(exercise);
    }
}
