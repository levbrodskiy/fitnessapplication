package ru.iei.fitnessapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iei.fitnessapp.controllers.responses.UserResponse;
import ru.iei.fitnessapp.db.entities.*;
import ru.iei.fitnessapp.db.repositories.FitnessStageRepository;
import ru.iei.fitnessapp.db.repositories.UserExerciseRepository;
import ru.iei.fitnessapp.db.repositories.UserFitnessStageRepository;
import ru.iei.fitnessapp.db.repositories.UserRepository;
import ru.iei.fitnessapp.services.exceptions.AuthorizationException;
import ru.iei.fitnessapp.services.exceptions.RegistrationException;
import ru.iei.fitnessapp.utils.ResponseUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserFitnessStageRepository userFitnessStageRepository;

    @Autowired
    private FitnessStageRepository fitnessStageRepository;

    @Autowired
    private UserExerciseRepository userExerciseRepository;


    public UserResponse getUserResponse(long userID) {
        User user = userRepository.getOne(userID);

        if (user == null) {
            return null;
        }

        return ResponseUtils.getUserResponse(user);
    }

    public List<UserFitnessStage> getUserStages(long userID) {
        return userFitnessStageRepository.findAllByUserId(userID);
    }

    @Transactional
    public User register(String username, String password, String complexityLevel) throws RegistrationException {
        try {

            User user = userRepository.findByUsername(username);

            if (user != null) {
                System.out.println("user!=null");
                throw new RegistrationException();
            }

            //ComplexityLevel level = ComplexityLevelUtils.getComplexityLevel(complexityLevel);

            User userNew = new User(username, password, ComplexityLevel.EASY);
            userRepository.saveAndFlush(userNew);
            userNew.setUserFitnessStages(getStages(fitnessStageRepository.findAll(), userNew));
            userRepository.saveAndFlush(userNew);
            return userNew;

        } catch (SecurityException e) {
            throw new RegistrationException();
        }
    }

    private List<UserFitnessStage> getStages(List<FitnessStage> stages, User user) {
        List<UserFitnessStage> result = new ArrayList<>();

        for (FitnessStage fitnessStage : stages) {
            UserFitnessStage stage = new UserFitnessStage();
            stage.setUser(user);
            stage.setStatus(FitnessStageStatus.IN_PROCESS);
            stage.setName(fitnessStage.getName());
            stage.setDescription(fitnessStage.getDescription());
            stage.setNumber(fitnessStage.getNumber());

            userFitnessStageRepository.saveAndFlush(stage);
            List<UserExercise> exerciseList = new ArrayList<>();

            for (Exercise exercise : fitnessStage.getExercises()) {
                UserExercise userExercise = new UserExercise();
                userExercise.setName(exercise.getName());
                userExercise.setDescription(exercise.getDescription());
                userExercise.setNumber(exercise.getNumber());
                userExercise.setUserFitnessStage(stage);
                userExerciseRepository.saveAndFlush(userExercise);
            }

            stage.setUserExercises(exerciseList);

            result.add(stage);
        }

        return result;
    }

    @Transactional
    public User authorize(String username, String password) throws AuthorizationException {
        try {
            User user = userRepository.findByUsername(username);

            if (user == null || !user.getPassword().equals(password)) {
                throw new AuthorizationException();
            }

            return user;
        } catch (Exception e) {
            throw new AuthorizationException();
        }
    }

}






