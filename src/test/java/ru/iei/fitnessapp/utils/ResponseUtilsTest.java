package ru.iei.fitnessapp.utils;

import org.junit.Assert;
import org.junit.Test;
import ru.iei.fitnessapp.controllers.responses.UserExerciseResponse;
import ru.iei.fitnessapp.controllers.responses.UserFitnessStageResponse;
import ru.iei.fitnessapp.controllers.responses.UserResponse;
import ru.iei.fitnessapp.db.entities.User;
import ru.iei.fitnessapp.db.entities.UserExercise;
import ru.iei.fitnessapp.db.entities.UserFitnessStage;

public class ResponseUtilsTest {

    @Test
    public void getStageResponse() {
        UserFitnessStage stage = new UserFitnessStage();
        stage.setId(3L);
        stage.setNumber(4);
        stage.setName("NAME");
        stage.setDescription("D");
        User user = new User();
        user.setId(7L);
        stage.setUser(user);
        UserFitnessStageResponse response = ResponseUtils.getStageResponse(stage);
        Assert.assertEquals(response.getUserID(), user.getId());
        Assert.assertEquals(response.getName(), stage.getName());
        Assert.assertEquals(response.getNumber(), stage.getNumber());
        Assert.assertEquals(response.getDescription(), stage.getDescription());
        Assert.assertEquals(response.getStageID(), stage.getId());
    }

    @Test
    public void getUserExerciseResponse() {
        UserExercise exercise = new UserExercise();
        exercise.setId(8L);
        exercise.setMissing(false);
        exercise.setNumber(5);
        exercise.setDescription("D");
        UserFitnessStage stage = new UserFitnessStage();
        stage.setId(5L);
        exercise.setUserFitnessStage(stage);

        UserExerciseResponse response = ResponseUtils.getUserExerciseResponse(exercise, 7L);

        Assert.assertEquals(response.getId(), exercise.getId());
        Assert.assertEquals(response.getMissing(), exercise.getMissing());
        Assert.assertEquals(response.getNumber(), exercise.getNumber());
        Assert.assertEquals(response.getDescription(), exercise.getDescription());
        Assert.assertEquals(response.getStageID(), stage.getId());
        Assert.assertEquals(response.getUserID(), new Long(7L));
    }

    @Test
    public void getUserResponse() {
        User user = new User();
        user.setId(5L);
        user.setUsername("email");
        UserResponse response = ResponseUtils.getUserResponse(user);
        Assert.assertEquals(response.getUserID(), user.getId());
        Assert.assertEquals(response.getEmail(), user.getUsername());
    }
}